from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    params = {
        "per_page": 1,
        "query": f'{city} {state}',
    }
    headers = {"Authorization": PEXELS_API_KEY}
    url = 'https://api.pexels.com/v1/search'

    response = requests.get(url , params=params, headers=headers)
    content = json.loads(response.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}

def get_weather_data(city, state):

    url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}'

    response = requests.get(url)
    content = json.loads(response.content)

    params = {
        "lat": content[0]["lat"],
        "lon": content[0]["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": 'imperial'
    }

    current_weather_url = 'https://api.openweathermap.org/data/2.5/weather'

    response2 = requests.get(current_weather_url, params=params)
    weather_content = json.loads(response2.content)

    try:
        return {
            'temp': weather_content["main"]["temp"],
            "description": weather_content["weather"][0]["description"]
        }
    except:
        return {
            'temp': None,
            "description": None
        }
